# Persistent Volumes : NFS
<br>

* installation du serveur nfs :

```
sudo apt-get install nfs-kernel-server

```

```
sudo mkdir -p /mnt/data

sudo nano /etc/exports

sudo /mnt/data 192.168.56.0/24(rw,sync,no_subtree_check)

sudo exportfs -a

sudo systemctl restart nfs-kernel-server

```
-----------------------------------------------------------

# Création du Persistent Volume
<br>

* manifeste PV :

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: data-pv
spec:
  storageClassName: manual
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteMany
  nfs:
    server: 192.168.56.106
    path: "/mnt/data"
```
-----------------------------------------------------------

# Création du Persistent Volume Claim
<br>

* manifest PVC :

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: data-pvc
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 500Mi
```
