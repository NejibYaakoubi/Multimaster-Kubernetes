
```
                                          6443  +------------+
                                                |            |
       example.local      +------------+   +---->  Master1   |
                          |            |   |    |            |
                    +-----+  HAPROXY   |   |    +------------+
                          |            |   |
                          +------------+   |
                          |            +---+    +------------+
                          |                |    |            |
                  80/443  |                +---->  Master2   |
                          |                     |            |
           +------------+ |    +------------+   +------------+
           |            | |    |            |
           |  worker01   <------>  worker02 |
           |            |      |            |
           +------------+      +------------+

```

------------------------------------------------------------------------
# configuration de  HAPROXY 
<br>

```
sudo nano /etc/haproxy/haproxy.cfg


frontend masters
  bind 192.168.56.200:6443
  mode tcp
  option tcplog
  default_backend masters
 
backend masters
    mode tcp
    option tcp-check
    balance roundrobin
    server master1 192.168.56.101:6443 check
    server master2 192.168.56.102:6443 check

frontend workers
  bind *:80
  mode tcp
  option tcplog
  default_backend workers
 
backend workers
    mode tcp
    option tcp-check
    balance roundrobin
    server worker1 192.168.56.103:80 check
    server worker2 192.168.56.104:80 check

listen stats
    bind 192.168.56.200:80
    mode http
    stats enable
    stats uri /stats
    stats auth admin:admin
    stats admin if TRUE

```
