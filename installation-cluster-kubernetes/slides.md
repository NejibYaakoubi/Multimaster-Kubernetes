#  Kubernetes : initialisation et join 
---------------------------------------------------------------------------------------
<br>

* initilisation sur le master1 :

```
kubeadm init --pod-network-cidr=192.168.0.0/16 --control-plane-endpoint="192.168.56.200:6443" \
             --upload-certs --apiserver-advertise-address=192.168.56.101

```
* création du fichier de configuration :
<br>

```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

```
--------------------------------------------------------------------------------------------------------------------------------------------------------------
-> Mise en place du réseau interne : choix de calico <-
<br>

```
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml

```
-------------------------------------------------------------------------------------------------------------------------------------------------------------

-> Kubernetes : join <-

* on fait le join sur le master2 :
<br>

```
kubeadm join 192.168.56.200:6443 --token ootzwz.2nh6sn272gvgytf2 \
	--discovery-token-ca-cert-hash sha256:cd0cddf0cd672edbc05e67dc924dbee5988583a67881b3859cdb2782934b3a61 \
	--control-plane --certificate-key 376d9a8bcfa28f172dd2374a4170729256eb27d0d0a5003d246e60fa97cc12dd \
        --apiserver-advertise-address=192.168.56.102
```
* on fait le join sur les workers :
<br>

```
kubeadm join 192.168.56.200:6443 --token ootzwz.2nh6sn272gvgytf2 \
	--discovery-token-ca-cert-hash sha256:cd0cddf0cd672edbc05e67dc924dbee5988583a67881b3859cdb2782934b3a61
```
* on vérifie l'état des pods system :
<br>

```
kubectl get pods --all-namespace
kubectl get nodes

```
