-> configuration réseau <-
<br>
* modification du nom d'hôte
<br>

```
sudo hostnamectl set-hostname haproxy

```
* l'ajout de l'@ip static pour se connecter en externe
<br>

```
sudo nano /etc/netplan/00-installer-config.yaml

network:
  ethernets:
    enp0s3:
      dhcp4: true
    enp0s8:
      addresses: [192.168.56.200/24]
  version: 2

```
* validation de l'ajout : 
<br>

```
sudo netplan apply

```
* association d'adresses IP à des noms d'hôtes :
<br>

```
sudo nano  /etc/hosts

192.168.56.200 haproxy
192.168.56.101 master1
192.168.56.102 master2
192.168.56.103 worker1
192.168.56.104 worker2

```
* Copier l'ID public sur le serveur distant : 
<br>

```
ssh-copy-id -i .ssh/id_rsa.pub master@haproxy

```
* connexion SSH 
<br>

```
ssh master@haproxy

```
* désactivation de l'authentification par mot de passe : 
<br>

```
sudo nano /etc/ssh/sshd_config

PasswordAuthentication no

sudo systemctl reload sshd
sudo systemctl enable sshd

```
-----------------------------------------------------------

# installation de HAProxy

sudo apt-get update && apt-get install haproxy -y
